import { FunctionComponent } from 'react';

import { AlertingService } from 'components/AlertingService';

import { BaseLayout } from './BaseLayout';


export const withLayout = <T extends Record<string, unknown>>(Component: FunctionComponent<T>) => {
  return function withLayoutComponent(props: T): JSX.Element {
    return (
      <BaseLayout>
        <AlertingService 
          horizontal={'right'}
          vertical={'top'}
        />
        <Component {...props} />
      </BaseLayout>
    );
  };
};
