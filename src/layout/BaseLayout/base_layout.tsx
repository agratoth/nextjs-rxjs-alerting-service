import { BaseLayoutProps } from './base_layout.props';
import styles from './base_layout.module.scss';


export const BaseLayout = ({ children }: BaseLayoutProps): JSX.Element => {
  return (
    <div className={ styles.default }>
      {children}
    </div>
  );
};
