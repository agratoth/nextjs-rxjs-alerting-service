import { AppProps } from 'next/app';
import React from 'react';
import Head from 'next/head';

import '../styles/globals.scss';


function App ({ Component, pageProps }: AppProps): JSX.Element {
  return (
    <>
      <Head>
        <title>NextJS+RxJS Alerting service</title>
        <link rel="apple-touch-icon" sizes="180x180" href="/static/favicons/apple-touch-icon.png" />
        <link rel="icon" type="image/png" sizes="32x32" href="/static/favicons/favicon-32x32.png" />
        <link rel="icon" type="image/png" sizes="16x16" href="/static/favicons/favicon-16x16.png" />
      </Head>
      <Component {...pageProps} />
    </>
  );
}

export default App;
