import { success, error, warning, info } from 'services/alerting_service';

import { withLayout } from 'layout';
import { Button } from 'components/Button';
import { CenteredVerticalForm } from 'components/CenteredVerticalForm';


const Home = (): JSX.Element => {
  return (
    <CenteredVerticalForm>
      NextJS+RxJS Alerting service
      <Button color={'success'} onClick={() => success('Success message', 3)}>Success</Button>
      <Button color={'error'} onClick={() => error('Error message', 10)}>Error</Button>
      <Button color={'warning'} onClick={() => warning('Warning message', 5)}>Warning</Button>
      <Button color={'info'} onClick={() => info('Info message')}>Info</Button>
    </CenteredVerticalForm>
  );
};

export default withLayout(Home);