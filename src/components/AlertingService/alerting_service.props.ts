export interface AlertingServiceProps {
  vertical: 'top'|'bottom';
  horizontal: 'left'|'right';
}