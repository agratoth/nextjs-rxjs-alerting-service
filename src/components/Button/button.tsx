import cl from 'classnames';

import { ButtonProps } from './button.props';
import styles from './button.module.scss';


export const Button = (props: ButtonProps): JSX.Element => {
  return (
    <button
      className={cl(
        styles.default,
        styles[props.color],
      )}
      onClick={props.onClick}
    >
      {props.children}
    </button>
  );
};