import { ButtonHTMLAttributes, DetailedHTMLProps, ReactNode } from 'react';
import { TColors } from 'types/colors';


export interface ButtonProps extends
	DetailedHTMLProps<ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement> {
	children: ReactNode;
  color: TColors;
}