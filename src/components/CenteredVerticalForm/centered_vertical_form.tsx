import { CenteredVerticalFormProps } from './centered_vertical_form.props';
import styles from './centered_vertical_form.module.scss';


export const CenteredVerticalForm = (props: CenteredVerticalFormProps): JSX.Element => {
  return (
    <div className={styles.container}>
      <div className={styles.form}>
        {props.children}
      </div>
    </div>
  );
};