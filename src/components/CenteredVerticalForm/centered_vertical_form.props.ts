import { ReactNode } from 'react';


export interface CenteredVerticalFormProps {
  children: ReactNode,
}