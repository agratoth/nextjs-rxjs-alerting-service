import cl from 'classnames';
import { useEffect } from 'react';

import { close } from 'services/alerting_service';

import { AlertProps } from './alert.props';
import styles from './alert.module.scss';


export const Alert = (props: AlertProps): JSX.Element => {
  useEffect(() => {
    if (props.timeout > 0){
      const timer = setTimeout(() => {
        close(props.id);
      }, props.timeout * 1_000);

      return () => {
        clearTimeout(timer);
      };
    }
    
  }, [props.id, props.timeout]);

  return (
    <div className={cl(
      styles.default,
      styles[props.status],
    )}>
      {props.message}
      <button onClick={() => close(props.id)}>X</button>
    </div>
  );
};